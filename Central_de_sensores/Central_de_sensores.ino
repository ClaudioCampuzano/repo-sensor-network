#include <OneWire.h>                
#include <DallasTemperature.h>
#include "DHT.h"
#include <SoftwareSerial.h>


#define RX 4
#define TX 5

//Sensor sumergible
OneWire ourWire(2);                //Se establece el pin 2  como bus OneWire
DallasTemperature sumergible(&ourWire); //Se declara una variable u objeto para nuestro sensor

//Sensor DHT22 AM2303
#define DHTPIN 3
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

//Soil moisture
#define SoilMoisture A0

int UVOUT = A5; //Output from the sensor
int REF_3V3 = A4; //3.3V power on the Arduino board

String AP = "crackqlo";    
String PASS = "1a2s3d4f5gh";
String API = "FQ5OFFWON717SEBP";   
String HOST = "api.thingspeak.com";
String PORT = "80";

int countTrueCommand;
int countTimeCommand; 

boolean found = false; 

unsigned long tiempo = 0;
unsigned long t_actualizado = 0;
unsigned long t_delay = 250;

SoftwareSerial esp8266(RX,TX); 


void setup() {

  pinMode(UVOUT, INPUT);
  pinMode(REF_3V3, INPUT);
  Serial.begin(9600);
  sumergible.begin();   //Se inicia el sensor
  dht.begin();

  esp8266.begin(115200);
  sendCommand("AT",5,"OK");
  sendCommand("AT+CWMODE=1",5,"OK");
  sendCommand("AT+CWJAP=\""+ AP +"\",\""+ PASS +"\"",20,"OK");
}
 
void loop() {

  tiempo = millis();
  if ( tiempo > t_actualizado + t_delay) {
    t_actualizado = tiempo;

    sumergible.requestTemperatures();   //Se envía el comando para leer la temperatura
    float temp_sumergible= sumergible.getTempCByIndex(0); //Se obtiene la temperatura en ºC
  
    float h_dht = dht.readHumidity();
    float t_dht = dht.readTemperature();
  
    int valorHumedad_map_soil = map(analogRead(SoilMoisture), 0, 1023, 100, 0);
  
    int uvLevel = averageAnalogRead(UVOUT);
    int refLevel = averageAnalogRead(REF_3V3);
    float outputVoltage = 3.3 / refLevel * uvLevel;  
    float uvIntensity = mapfloat(outputVoltage, 0.99, 2.9, 0.0, 15.0);
  
    String getData = "GET /update?api_key="+ API;
    getData += "&field1="+String(temp_sumergible);
    getData += "&field2="+String(valorHumedad_map_soil);
    getData += "&field3="+String(uvIntensity);
    getData += "&field4="+String(h_dht);
    getData += "&field5="+String(t_dht);
    sendCommand("AT+CIPMUX=1",5,"OK");
    sendCommand("AT+CIPSTART=0,\"TCP\",\""+ HOST +"\","+ PORT,15,"OK");
    sendCommand("AT+CIPSEND=0," +String(getData.length()+4),4,">");
    esp8266.println(getData);
    countTrueCommand++;
    sendCommand("AT+CIPCLOSE=0",5,"OK");
  
  /*
    Serial.print(F("Temperatura agua= "));
    Serial.print(temp_sumergible);
    Serial.println(F(" °C"));
  
    Serial.print(F("Humedad ambiente: "));
    Serial.print(h_dht);
    Serial.print(F("%  Temperatura ambiente: "));
    Serial.println(t_dht);
  
    Serial.print(F("Sensor humedad suelo: "));
    Serial.println(valorHumedad_map_soil);
   
    Serial.print(F("Sensor UV (mW/cm^2):: "));
    Serial.println(uvIntensity);
    
    Serial.println("---------------------------------------------------");*/
  }
}

int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 
  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;
  return(runningValue);  
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void sendCommand(String command, int maxTime, char readReplay[]) {
  /*Serial.print(countTrueCommand);
  Serial.print(". at command => ");
  Serial.print(command);
  Serial.print(" ");*/
  while(countTimeCommand < (maxTime*1)){
    esp8266.println(command);//at+cipsend
    if(esp8266.find(readReplay)){
      found = true;
      break;
    }
    countTimeCommand++;
  }
  if(found == true){
    //Serial.println("OK");
    countTrueCommand++;
    countTimeCommand = 0;
  }
  if(found == false){
    //Serial.println("Fail");
    countTrueCommand = 0;
    countTimeCommand = 0;
  }
  found = false;
}
