//#include "Seeed_BME280.h"
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "CrackAP";
const char* password = "holamundo";
const char* mqtt_server = "192.168.4.1";
WiFiClient espClient;
PubSubClient client(espClient);

BME280 bme280;
int UVsensorIn = A0; //Output from the sensor

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected - ESP IP address: ");
  Serial.println(WiFi.localIP());
}

int averageAnalogRead(int pinToRead)
{
  byte numberOfReadings = 8;
  unsigned int runningValue = 0; 
 
  for(int x = 0 ; x < numberOfReadings ; x++)
    runningValue += analogRead(pinToRead);
  runningValue /= numberOfReadings;
 
  return(runningValue);  
}

float mapfloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void setup()
{
  Serial.begin(9600);
  if(!bme280.init()){
    Serial.println("Device error!");
  }
  pinMode(UVsensorIn, INPUT);
  Serial.begin(9600); //open serial port, set the baud rate to 9600 bps

  setup_wifi();
  client.setServer(mqtt_server, 1883);
}

void loop()
{
  float pressure, temperature, altitude, humidity, uvIntensity;
  int uvLevel = averageAnalogRead(UVsensorIn);
  float outputVoltage = 3.3 * uvLevel/1024;

  if(!client.loop())
    client.connect("ESP8266Client");
  /*
  temperature = bme280.getTemperature();
  pressure = bme280.getPressure();
  altitude = bme280.calcAltitude(pressure);
  humidity = bme280.getHumidity();
  uvIntensity = mapfloat(outputVoltage, 0.99, 2.9, 0.0, 15.0);
  */
  //get and print temperatures
  Serial.print("Temp: ");
  Serial.print(temperature);
  Serial.println("[C]");//The unit for  Celsius because original arduino don't support special symbols
  
  //get and print atmospheric pressure data
  Serial.print("Pressure: ");
  Serial.print(pressure);
  Serial.println("[Pa]");

  //get and print altitude data
  Serial.print("Altitude: ");
  Serial.print(altitude);
  Serial.println("[m]");

  //get and print humidity data
  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.println("%");

  Serial.print("UV Intensity: ");
  Serial.print(uvIntensity);
  Serial.println("[mW/cm^2]"); 
  
  Serial.println(); 

  delay(1000);

  static char temperature_[7];
  static char pressure_[7];
  static char altitude_[7];
  static char humidity_[7];
  static char uvIntensity_[7];
  
  dtostrf(temperature, 6, 1, temperature_);
  dtostrf(pressure, 6, 2, pressure_);
  dtostrf(altitude, 6, 2, altitude_);
  dtostrf(humidity, 3, 2, humidity_);
  dtostrf(uvIntensity, 2, 2, uvIntensity_);

  client.publish("room/Temperature",temperature_);
  client.publish("room/Pressure",pressure_);
  client.publish("room/Altitude",altitude_);
  client.publish("room/Humidity",humidity_);
  client.publish("room/Uv_intensity",uvIntensity_);
}


 
 
